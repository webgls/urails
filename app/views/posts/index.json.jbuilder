json.array!(@posts) do |post|
  json.extract! post, :id, :title, :body, :isPublished
  json.url post_url(post, format: :json)
end
